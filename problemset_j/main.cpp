#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

int main() {
    int value_n, value_m, previous_x = 0, current_x = 0, cumulative_total = 0, current_counter = 1;
    bool is_unique = true;
    std::vector<int> elements, uniques;
    std::cin >> value_n >> value_m;

    for (int i = 0; i < value_n; i++) {
        std::cin >> current_x;

        if (current_x > 0) {
            if (i > 0)  {
                for (int item : elements) {
                    if (current_x == item) {
                        is_unique = false;
                        break;
                    } else {
                        is_unique = true;
                    }
                }
            }

            if (is_unique) {
                elements.push_back(current_x);
            }
        }
    }

    for (auto &item : elements) {
        if (previous_x < item) {
            previous_x = item;
        }

        if (current_counter == value_m || elements.back() == item) {
            cumulative_total += previous_x;
            current_counter = 1;
        } else {
            current_counter++;
        }

    }

    std::cout << cumulative_total << std::endl;

    return 0;
}
