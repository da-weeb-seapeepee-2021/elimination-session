#include <iostream>

using namespace std;

int main() {
    long long int N, X, min = 0, max = 0, angka;
    cin >> N >> X;

    for (int i=0 ; i<N; i++){
        cin >> angka;

        if (X > angka){
            if (min == 0){
                min = angka;
            }
            else if (min < angka){
                min = angka;
            }
        }
        else if(X < angka){
            if (max == 0){
                max = angka;
            }
            else if (max > angka){
                max = angka;
            }
        }
        else if (X == angka){
            cout << angka<<endl;
            min = 0;
            max = 0;
            break;
        }
    }

    if ((X - min) < (max - X) && min !=0 && max !=0){
        cout << min << endl;
    }

    if ((X - min) > (max - X) && min !=0 && max !=0){
        cout << max << endl;
    }

    if ((X - min) == (max - X) && min !=0 && max !=0){
        cout << min << endl << max << endl;
    }
}
